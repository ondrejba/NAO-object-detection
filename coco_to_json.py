import tensorflow as tf
from google.protobuf import text_format

with open("object_detection/data/mscoco_label_map.pbtxt") as f:
    txt = f.read()

graph_def = text_format.Parse(txt, tf.GraphDef())

for node in graph_def.node:
	print(node.name)
