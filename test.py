#!/usr/bin/env python
# -*- coding: utf-8 -*-

from naoqi import ALProxy

IP = "10.10.48.221"
PORT = 9559

tts = ALProxy("ALTextToSpeech", IP, PORT)
tts.setLanguage("Czech")
tts.setParameter("speed", 400)

tts.say("řeřicha")

