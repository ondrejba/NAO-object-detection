import numpy as np
import matplotlib
import matplotlib.pyplot as plt
plt.style.use("seaborn-colorblind")

def fc(d):
	y = np.abs(np.sqrt( 4 / (float(d) ** 2) + 4) / (2 * (float(d) ** 2 + 1) / float(d)))
	x = float(d) * y
	return x, y

x1 = np.concatenate([np.linspace(-1.0, -0.001, 1000), np.linspace(0.001, 1.0, 1000)], axis=0)
y1 = np.ones(x1.shape[0], dtype=np.float32)
points = np.array([fc(d) for d in x1], dtype=np.float32)

circle = plt.Circle((0, 0), 1, fill=False)

plt.figure(figsize=(10, 5))
plt.scatter(x1, y1, label="source coordinates")
plt.scatter(points[:, 0], points[:, 1], label="projected coordinates")

ax = plt.gca()
ax.add_artist(circle)

plt.xlim(-1.1, 1.1)
plt.ylim(0.0, 1.1)

plt.xticks([-1.0, 0.0, 1.0])
plt.yticks([0.0, 1.0])

plt.legend()

plt.show()

