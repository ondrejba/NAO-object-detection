import qi 
import vision_definitions
import cv2
import numpy as np


def sub_depth(video_device):
	return vs.subscribeCamera("python_depth", 2, vision_definitions.kQVGA, vision_definitions.kYuvColorSpace, 1)

def sub_head(video_device):
	return vs.subscribeCamera("python_rgb", 0, vision_definitions.kQVGA, vision_definitions.kRGBColorSpace, 1)

def unsub(vs, name_id):
	vs.unsubscribe(name_id)

def process_image(raw):
	width, height = raw[0], raw[1]
	arr = np.fromstring(str(raw[6]), np.uint8)
	depth = int(np.product(arr.shape) / (width * height))
	arr = arr.reshape((height, width, depth))
	return arr

def roll_image_bottom_left(image, roll_left, roll_bottom):

	new_image = np.zeros_like(image)
	new_image[:new_image.shape[0] - roll_bottom, roll_left:, :] = image[roll_bottom:, :image.shape[1] - roll_left, :]

	return new_image

def show_depth(depth):

	depth = depth / 255.0
	depth = depth[:, :, 0]

	cv2.imshow("image", depth)
	cv2.waitKey(0)

def show_rgb(rgb):

	rgb = rgb / 255.0

	cv2.imshow("image", rgb)
	cv2.waitKey(0)


session = qi.Session()
session.connect("tcp://10.10.48.230:9559")

vs = session.service("ALVideoDevice")

for idx in vs.getCameraIndexes():
  print(idx, vs.getCameraName(idx))

# subscribe cameras
depth_id = sub_depth(vs)
rgb_id = sub_head(vs)

# get rgb and depth images
depth_raw = vs.getImageRemote(depth_id)
rgb_raw = vs.getImageRemote(rgb_id)

print("angles:")
print(rgb_raw[8], rgb_raw[9], rgb_raw[10], rgb_raw[11])

# unsubscribe cameras
unsub(vs, depth_id)
unsub(vs, rgb_id)

# show images
rgb = process_image(rgb_raw)
rgb = roll_image_bottom_left(rgb, 0, 10)
depth = process_image(depth_raw)

image = np.zeros((rgb.shape[0], rgb.shape[1], 3), dtype=np.float32)
image[:, :, 0] = rgb[:, :, 0] / 255.0
image[:, :, 2] = depth[:, :, 0] / 255.0

cv2.imshow("image", image)
cv2.waitKey(0)

show_depth(depth)
show_rgb(rgb)

