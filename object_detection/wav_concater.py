import wave

class wavConcater:
	
	def concate(self, infiles, outfile):
		
		frame_sum = 0
		data= []
		for infile in infiles:
			w = wave.open(infile, 'rb')
			#data.append([w.getparams(), w.readframes(w.getnframes())])
			tmp_data = [w.getparams(), w.readframes(w.getnframes())]
			print(tmp_data[1])
			print("=================")
			w.close()
			
			counter = 1
			for i in reversed(tmp_data[1]):
				if i != 0:
					break
				counter += 1
			frame_sum += len(tmp_data[1])-counter
			tmp_data[1] = tmp_data[1][:-26000]
			data.append(tmp_data)
			print(counter)
			
		output = wave.open(outfile, 'wb')
		output.setparams(data[0][0])
		output.setnframes(frame_sum)
		for i in data:
		    output.writeframes(i[1])
		output.close()
