# NAO Robot object detection

**NAO Robot real-time object detection and recognition using Tensorflow Object Detection API.**

This project was as a final semestral project for subject BI-ZIVS (Intelligent Embedded System Fundamentals) at FIT CTU university.

## Getting Started

These instructions will get you a copy of the project up and running on your local machine for development and testing purposes.

### Prerequisites

To run this project you'll need few things to install beforehand.

* Run `apt-get update && apt-get upgrade` to get newest possible packages.

* Install `apt-get install python-dev` so you can compile python libraries.

* Install protobuf-compiler:  
`apt-get install protobuf-compiler`

* Install Tensorflow:  
`pip install https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.4.0-cp27-none-linux_x86_64.whl`

* And make sure you have Tensorflow dependencies by running:  
`pip install funcsigs pbr pillow lxml`

* If you don't have OpenCV installed also install it by issuing:  
`pip install opencv-python`


### Installing

Download and configure the project.

* Simply clone this repository:  
`git clone https://gitlab.com/hokyjack/NAO-object-detection.git`

* Switch to project folder:  
`cd NAO-object-detection/`

* Download COCO-pretrained model:  
`wget http://download.tensorflow.org/models/object_detection/ssd_mobilenet_v1_coco_2017_11_17.tar.gz`

* And extract it to **trained_models** folder:  
`mkdir -p trained_models && tar -xvf ssd_mobilenet_v1_coco_2017_11_17.tar.gz -C trained_models`

* Lastly compile the protobuf libraries:  
`protoc object_detection/protos/*.proto --python_out=.`

Now you should be ready to go!

## Installing on Pepper

ssh nao@10.10.48.230
pip install https://storage.googleapis.com/tensorflow/linux/cpu/tensorflow-1.4.0-cp27-none-linux_x86_64.whl

## Running

You can run script with default parameters:  
`python main.py`

Or you can edit some parameters modifying variables in top of the file first (I will add run arguments later):
```
robot_IP = "10.10.48.252"  # NAO robot ip
robot_PORT = 9559  # NAO robot port
camera_id_device = 0 # which webcam/device to use (eg. 0 for NAO top, 1 NAO bottom)
resolution = 160  # 640 or 320 or 160
score_thresh = 0.4  # object detection bellow this threshold will be ignored
say_interval = 10  # after how many seconds it will print / say output

# debug options
local_webcam_debug = True # use local webcam (eg. if you don't own NAO robot)
debug_log = False # whether to print FPS log
```

## Authors

* **Jan Horák** - *Initial work* - [hokyjack](https://gitlab.com/hokyjack/)

## License

This project is licensed under the MIT License - see the [LICENSE.md](LICENSE.md) file for details

## Acknowledgments

* For NAO Video stream I modified **Sean Guo's** [awesome NAO library](https://github.com/SeanXP/Nao-Robot/blob/master/python/vision/VideoDevice/video_image.py)
