from naoqi import ALProxy

import random
import time
import sys
import threading
import numpy as np

class VideoImage(threading.Thread):

	def __init__(self, robot_ip, robot_port=9559):
		threading.Thread.__init__(self)
		self.setDaemon(True)
		self.nameID = "video_image_" + str(random.randint(0,100))

		# Camera indexes
		self.TopCamera = 0
		self.BottomCamera = 1
		self.cameraIndex = self.TopCamera

		#	kQQVGA (160x120), kQVGA (320x240), kVGA (640x480) or k4VGA (1280x960, only with the HD camera).
		# (Definitions are available in alvisiondefinitions.h)
		self.RESOLUTION_QQVGA_160 = 0		# 1/4 VGA
		self.RESOLUTION_QVGA_320 = 1			# 1/2 VGA
		self.RESOLUTION_VGA_640 = 2			# VGA, 640x480
		self.resolution = self.RESOLUTION_QVGA_320

		# (ColorSpace):
		# kYuvColorSpace, kYUVColorSpace, kYUV422ColorSpace (9), kRGBColorSpace(11), etc.
		# (Definitions are available in alvisiondefinitions.h)
		ColorSpace_YUV422 = 9	# 0xVVUUYY
		ColorSpace_YUV = 10		# 0xY'Y'VVYY
		ColorSpace_RGB = 11		# 0xBBGGRR
		ColorSpace_BGR = 13		# 0xRRGGBB
		self.colorSpace = ColorSpace_RGB;

		self.fps = 30;

		self.subscriberID = None
		self.subscriberID_Top = None
		self.subscriberID_Bottom = None

		self.show_flag = False

		self.frameWidth = 320
		self.frameHeight = 240

		# naoqi.ALProxy
		try:
			self.video = ALProxy("ALVideoDevice", robot_ip, robot_port)
		except Exception, e:
			print "class VideoImage::__init__() Could not create proxy by ALProxy"
			print "Error: ", e

	def run(self):
		pass

	def stop(self):
		pass

	def close(self):
		self.stop()
		time.sleep(1)
		self.unsubscribeCamera()

	def setCamera(self, index):
		if index == self.TopCamera:
		   print '<VideoImage> - Set Top Camera'
		   self.cameraIndex = self.TopCamera
		   self.subscriberID = self.subscriberID_Top
		elif index == self.BottomCamera:
		   print '<VideoImage> - Set Bottom Camera'
		   self.cameraIndex = self.BottomCamera
		   self.subscriberID = self.subscriberID_Bottom
		else:
		   print 'class VideoImage::setCamera() - Error Camera Index.'

	def switchCamera(self):
		if self.cameraIndex == self.TopCamera:
			self.setCamera(self.BottomCamera)
		else:
			self.setCamera(self.TopCamera)

	def getCamera(self):
		return self.cameraIndex

	def setResolution(self, resolution):
		if resolution == 320:
			self.resolution = self.RESOLUTION_QVGA_320
			self.frameWidth = 320
			self.frameHeight = 240
		elif resolution == 160:
			self.resolution = self.RESOLUTION_QQVGA_160
			self.frameWidth = 160
			self.frameHeight = 120
		elif resolution == 640:
			self.resolution = self.RESOLUTION_VGA_640
			self.frameWidth = 320
			self.frameHeight = 160
		else:
			pass

	def setColorSpace(self, colorspace):
		pass

	def setFPS(self, fps):
		if fps > 0 and fps <= 30:
			self.fps = fps
		else:
			print 'class VideoImage::setFPS() - Error fps'

	def setShowFlag(self, bools):
		self.show_flag = bools

	def subscribeCamera(self):
		# You only have to call the "subscribe" function with those parameters and
		# ALVideoDevice will be in charge of driver initialization and buffer's management.
		self.subscriberID_Top = self.video.subscribeCamera(	self.nameID,
										self.TopCamera,
										self.resolution,
										self.colorSpace,
										self.fps)
		self.subscriberID_Bottom = self.video.subscribeCamera(	self.nameID,
										self.BottomCamera,
										self.resolution,
										self.colorSpace,
										self.fps)

	def unsubscribeCamera(self):
		# Release image buffer locked by getImageLocal().
		# If module had no locked image buffer, does nothing.
		if self.subscriberID_Top != None:
			self.video.releaseImage(self.subscriberID_Top)
			self.video.unsubscribe(self.subscriberID_Top)
			self.subscriberID_Top = None
		if self.subscriberID_Bottom != None:
			self.video.releaseImage(self.subscriberID_Bottom)
			self.video.unsubscribe(self.subscriberID_Bottom)
			self.subscriberID_Bottom = None

	def getImageRemote(self):
		return self.video.getImageRemote(self.subscriberID)

	def getFrame(self):
		image = np.zeros((self.frameHeight, self.frameWidth, 3), np.uint8)

		result = self.video.getImageRemote(self.subscriberID);
		if result == None:
			print 'cannot capture.'
		elif result[6] == None:
			print 'no image data string.'
		else:
			# translate value to mat
			values = map(ord, list(result[6]))
			i = 0
			for y in range(0, self.frameHeight):
				for x in range(0, self.frameWidth):
					image.itemset((y, x, 0), values[i + 0])
					image.itemset((y, x, 1), values[i + 1])
					image.itemset((y, x, 2), values[i + 2])
					i += 3
			return image

	def getImageInfo(self):
		image = self.video.getImageRemote(self.subscriberID)
		# image : ALImage
		# image[0] : [int] width of the image
		# image[1] : [int] height of the image
		# image[2] : [int] number of layers of the image
		# image[3] : [int] colorspace of the image
		# image[4] : [int] time stamp in second
		# image[5] : [int] time stamp in microsecond (and under second)
		# image[6] : [int] data of the image
		# image[7] : [int] camera ID
		# image[8] : [float] camera FOV left angle (radian)
		# image[9] : [float] camera FOV top angle (radian)
		# image[10]: [float] camera FOV right angle (radian)
		# image[11]: [float] camera FOV bottom angle (radian)

		# image Info
		print "width of the image:		", image[0]
		print "height of the image:		", image[1]
		print "image.getNbLayers:		", image[2]
		print 'colorspace of the image:	', image[3]
		print 'time stamp (second):		', image[4]
		print 'time stamp (microsecond):', image[5]
		print 'data of the image (ignore to print). - image[6]', len(image[6])
		print 'camera ID:				', image[7]
		print 'camera FOV left angle:	', image[8]
		print 'camera FOV top angle:	', image[9]
		print 'camera FOV right angle:	', image[10]
		print 'camera FOV bottom angle:	', image[11]
