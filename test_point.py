from naoqi import ALProxy
import time

IP = "10.10.48.230"

posture = ALProxy("ALRobotPosture", IP, 9559)
posture.goToPosture("Stand", 1.0)

tracker = ALProxy("ALTracker", IP, 9559)

#for i in range(-12, 13):
#	position = [1.5, float(i) / 10., 1.]
#	tracker.pointAt("LArm", position, 2, 0.5)
#	time.sleep(1)

for j in [-20, -15, -10, -5, -4, -3, -2]:
	i = 10
	k = 0		
	print(i, j, k)
	position = [float(i) / 10, float(j) / 10, float(k) / 10]
	tracker.pointAt("RArm", position, 0, 0.5)
	time.sleep(5)

for j in [2, 3, 4, 5, 10, 15, 20]:
	i = 10
	k = 0		
	print(i, j, k)
	position = [float(i) / 10, float(j) / 10, float(k) / 10]
	tracker.pointAt("LArm", position, 0, 0.5)
	time.sleep(5)

# [-10, -3] ... RArm
# [3, 10] ... LArm 
