#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
import cv2
import time
import numpy as np
import tensorflow as tf
import random
import math

import threading

from utils.utils import FPS, WebcamVideoStream

from object_detection.utils import label_map_util
from object_detection.utils import visualization_utils as vis_util

ENGLISH = "english"
CZECH = "czech"

robot_IP = "10.10.48.230"  # NAO robot ip
robot_PORT = 9559  # NAO robot port
camera_id_device = 0 # which webcam/device to use (eg. 0 for NAO top, 1 NAO bottom)
resolution = 160  # 640 or 320 or 160
score_thresh = 0.4  # object detection bellow this threshold will be ignored
say_interval = 10  # after how many seconds it will print / say output
language=CZECH

# debug options
local_webcam_debug = False # use local webcam (eg. if you don't own NAO robot)
debug_log = False # whether to print FPS log


if not local_webcam_debug:
    from utils.nao_video import VideoImage
    from naoqi import ALProxy

    # initialization of ALProxy - TextToSpeech
    tts = ALProxy("ALTextToSpeech", robot_IP, robot_PORT)
    tracker = ALProxy("ALTracker", robot_IP, robot_PORT)

    if language == ENGLISH:
        tts.setLanguage("English")
    else:
        tts.setLanguage("Czech")

    posture = ALProxy("ALRobotPosture", robot_IP, robot_PORT)
    #auto_life = ALProxy("ALAutonomousLife", robot_IP, robot_PORT)
    #auto_life.setState("disabled")


CWD_PATH = os.getcwd()
TF_MODELS_PATH = os.path.join(CWD_PATH, 'trained_models')
# Path to frozen detection graph. This is the actual model that is used for the object detection
MODEL_NAME = 'ssd_mobilenet_v1_coco_2017_11_17'
"""Models available:
    ssd_mobilenet_v1_coco_2017_11_17 -> Fast - 21 mAP
"""
PATH_TO_CKPT = os.path.join(TF_MODELS_PATH, MODEL_NAME, 'frozen_inference_graph.pb')

# List of the strings that is used to add correct label for each box
if language == ENGLISH:
    PATH_TO_LABELS = os.path.join(CWD_PATH, 'object_detection', 'data', 'mscoco_label_map.pbtxt')
else:
    PATH_TO_LABELS = os.path.join(CWD_PATH, 'object_detection', 'data', 'mscoco_label_map_cz.pbtxt')

NUM_CLASSES = 90

# Loading label map
label_map = label_map_util.load_labelmap(PATH_TO_LABELS)
categories = label_map_util.convert_label_map_to_categories(label_map, max_num_classes=NUM_CLASSES,
                                                            use_display_name=True)
category_index = label_map_util.create_category_index(categories)

def model_load_into_memory():
    detection_graph = tf.Graph()
    with detection_graph.as_default():
        od_graph_def = tf.GraphDef()
        with tf.gfile.GFile(PATH_TO_CKPT, 'rb') as fid:
            serialized_graph = fid.read()
            od_graph_def.ParseFromString(serialized_graph)
            tf.import_graph_def(od_graph_def, name='')
    return detection_graph

def detect_objects(image_np, sess, detection_graph, min_score_thresh):
    # Expand dimensions since the model expects images to have shape: [1, None, None, 3]
    image_np_expanded = np.expand_dims(image_np, axis=0)
    image_tensor = detection_graph.get_tensor_by_name('image_tensor:0')

    # Each box represents a part of the image where a particular object was detected
    boxes = detection_graph.get_tensor_by_name('detection_boxes:0')

    # Each score represent how level of confidence for each of the objects.
    # Score is shown on the result image, together with the class label.
    scores = detection_graph.get_tensor_by_name('detection_scores:0')
    classes = detection_graph.get_tensor_by_name('detection_classes:0')
    num_detections = detection_graph.get_tensor_by_name('num_detections:0')

    # Actual detection
    (boxes, scores, classes, num_detections) = sess.run(
        [boxes, scores, classes, num_detections],
        feed_dict={image_tensor: image_np_expanded})

    # Print every class + probability prediction
    # Here output the category as string and score to terminal

    sq_boxes = np.squeeze(boxes)
    sq_scores = np.squeeze(scores)

    objects = [category_index.get(i) for i in classes[0]]
    output_objects = []

    for i, object in enumerate(objects):
        box = sq_boxes[i]
        score = sq_scores[i]
        if score < min_score_thresh:
            break

        # box = [ymin, xmin, ymax, xmax]
        top = True if (box[0] < 0.25 and box[2] < 0.6) else False
        bottom = True if (box[0] > 0.4 and box[2] > 0.75) else False
        left = True if (box[1] < 0.25 and box[3] < 0.6) else False
        right = True if (box[1] > 0.4 and box[3] > 0.75) else False
        middle = True if not (left or right or top or bottom) else False

        # example position
        pos_bools = [left, right, top, bottom, middle]

        if language == ENGLISH:
            pos_strings = [ "left", "right", "top", "bottom", "middle"]
        else:
            vlevo = [u"vlevo", u"nalevo"]
            vpravo = [u"vpravo", u"napravo"]
            nahore = [u"nahoře"]
            dole = [u"na úrovni očí"]

            uprostred = [u"uprostřed", u"na středu"] 
            pos_strings = [random.choice(vlevo),
                                    random.choice(vpravo),
                                    random.choice(nahore),
                                    random.choice(dole),
                                    random.choice(uprostred)]

        loc_subset = [pos_strings[i] for i, pos_bool in enumerate(pos_bools) if pos_bool]

        output_objects.append({
            "id": object["id"],
            "name": object["name"],
            "box": box,
            "score": score,
            "location": " ".join(loc_subset)
        })

    # Visualization of the results of a detection
    vis_util.visualize_boxes_and_labels_on_image_array(
        image_np,
        np.squeeze(boxes),
        np.squeeze(classes).astype(np.int32),
        np.squeeze(scores),
        category_index,
        use_normalized_coordinates=True,
        min_score_thresh=min_score_thresh,
        line_thickness=8)

    return output_objects, image_np

def format_objects(objects):
    n = len(objects)
    if n > 1:
        return (u'{}, ' * n).format(*objects)
    elif n > 0:
        return objects[0]
    else:
        return ''

def translate_and_say(text_to_say, local_webcam_debug):
    print(text_to_say)
    # NAO synthetise speach
    if not local_webcam_debug:
        tts.say(text_to_say)

def project(d):
    y = np.abs(np.sqrt( 4 / (float(d) ** 2) + 4) / (2 * (float(d) ** 2 + 1) / float(d)))
    x = float(d) * y
    return x, y

def translate_and_say_and_point(text_to_say, points_to_point, arms_to_point, local_webcam_debug):

    splits = text_to_say.split(",")    
    
    print(splits, points_to_point, arms_to_point)
    #assert(len(splits) == len(points_to_point) == len(arms_to_point))

    for point, arm, tmp_text in zip(points_to_point, arms_to_point, splits):

        y, x = project(point * 0.7)

        print("Saying {}".format(tmp_text))

        if not local_webcam_debug:
            tts.say(tmp_text)

        print("Pointing at ({:.2f},{:.2f}) => ({:.2f},{:.2f})".format(1.0, point, x, y))

        tracker.pointAt(arm, [x, y, 0.5], 0, 0.5)

        time.sleep(1)

        posture.goToPosture("Stand", 1.0)


def translate_to_arm_angle_width(point):

	res = - ((float(point) * 2.0) - 1.0)

	if res <= 0:
		hand = "RArm"
		res = min(res, -0.3)
	else:
		hand = "LArm"
		res = max(res, 0.3)		

	#res = (2.6*float(point))-1.3
	print("Vypocet: vstup {} a vystup ({}, {})".format(point, res, hand))
	return res, hand


def main(ip="10.10.48.252",
         port=9559,
         camera_id_device=0,
         resolution=160,
         min_score_thresh=0.4,
         say_interval=say_interval,
         localWebcam=False,
         debug_log=True):

    if localWebcam:
        video = WebcamVideoStream(src=camera_id_device, width=480, height=360).start()
    else:
        video = VideoImage(ip, port)
        video.setResolution(resolution)
        video.subscribeCamera()
        video.setCamera(camera_id_device)	# top camera
        # video.getImageInfo()

    fps = FPS().start()
    say_time = 5

    detection_graph = model_load_into_memory()
    with detection_graph.as_default():
        with tf.Session(graph=detection_graph) as sess:
            while True:  # Camera detection loop
                if localWebcam:
                    frame = video.read()
                else:
                    frame = video.getFrame()

                #cv2.imshow('Input', frame)
                t = time.time()
                output_objects, output_frame = detect_objects(
                    frame, sess, detection_graph, min_score_thresh)
                #cv2.imshow('Video', output_frame)
                
                #cv2.imwrite("{}_orig.png".format(t), output_frame)
                #cv2.imwrite("{}_boxes.png".format(t), output_frame)

                ## Lets say what we see every say_interval seconds (or on some input)
                if (time.time() > say_time):
                    say_time = time.time() + say_interval
                    if len(output_objects) > 0:
                        objects_desc = [u"{} {}".format(o["name"], o["location"])
                                        for o in output_objects]

			box = output_objects[0]["box"]
			print(box)

                        if language == ENGLISH:
                            text_to_say = u"I see {}.".format(format_objects(objects_desc))
                        else:
                            vidim = [u"Vidím", u"Já vidím"]
                            text_to_say = u"{} {}".format(random.choice(vidim), format_objects(objects_desc))
                    else:
                        if language == ENGLISH:
                            text_to_say = u"I don't recognize any object!"
                        else:
                            czech_texts_to_say = [u"Nic nevidím. Ukaž mi něco!",
                                                                u"Tady není nic zajímavého. Ukaž mi něco!",
                                                                u"Ukaž mi něco a já ti řeknu, co to je.",
                                                                u"Pojď si hrát! Ukaž mi něco!",
                                                                u"Rád ukazuju na věci.",
                                                                u"Rád popisuji věci."]
                            text_to_say = random.choice(czech_texts_to_say)
		    
                    arm_points = []
                    arms = []
		    for obj in output_objects:
			obj_name = obj["name"]
			obj_pic = obj["box"]
			obj_arm, obj_hand = translate_to_arm_angle_width(float((obj["box"][3]+obj["box"][1]))/2.0)
                    	arm_points.append(obj_arm)
			arms.append(obj_hand)
                        #print(u"Objekt {} z obrazkove pozice {} preveden na Xovou {}".format(obj_name, obj_pic, obj_arm))
		    print("Arm obj {}".format(arm_points))

		    thread = threading.Thread(target=translate_and_say_and_point, args=(text_to_say.encode("utf-8"), arm_points, arms, localWebcam))
                    #thread = threading.Thread(target=translate_and_say, args=(text_to_say.encode("utf-8"), localWebcam))
                    thread.start()
  		    thread.join()
		    

                fps.update()
                if debug_log:
                    print('[INFO] elapsed time: {:.2f}'.format(time.time() - t))

                #if cv2.waitKey(1) & 0xFF == ord('q'):
                #    break

            # end stream
            if localWebcam:
                video.stop() # stop webcam stream
            else:
                video.close() # close NAO video stream

            fps.stop()
            if debug_log:
                print('[INFO] elapsed time (total): {:.2f}'.format(fps.elapsed()))
                print('[INFO] approx. FPS: {:.2f}'.format(fps.fps()))
            cv2.destroyAllWindows()


if __name__ == '__main__':
    main(robot_IP, robot_PORT, camera_id_device, resolution, score_thresh, say_interval,
         local_webcam_debug, debug_log)
