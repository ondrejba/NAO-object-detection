#!/usr/bin/env python
# -*- coding: utf-8 -*-

import os
from naoqi import ALProxy
import object_detection.dict.items as dict_items

czech_texts = {
	100: {
		"czech": ["nahoře"], 
		"english": ["top"],
		"file_name": "top"
	},
	101: {
		"czech": ["dole"], 
		"english": ["bottom"],
		"file_name": "bottom"
	},
	102: {
		"czech": ["vlevo"], 
		"english": ["left"],
		"file_name": "left"
	},
	103: {
		"czech": ["vpravo"], 
		"english": ["right"],
		"file_name": "right"
	},
	104: {
		"czech": ["uprostřed"], 
		"english": ["middle"],
		"file_name": "middle"
	},
	105: {
		"czech": ["vidím"], 
		"english": ["I see"],
		"file_name": "see"
	},
	106: {
		"czech": ["Nic nevidím. Ukaž mi něco!"], 
		"english": ["I don't recognize any objects"],
		"file_name": "nothing"
	}
}

if __name__ == "__main__":

	coco_texts = dict_items.get_words_dictionary()
	for key, value in coco_texts.items():
		assert key not in czech_texts		
		czech_texts[key] = value

	folder_path = "/home/nao/audio"

	IP = "10.10.48.221"
	PORT = 9559

	tts = ALProxy("ALTextToSpeech", IP, PORT)
	tts.setLanguage("Czech")

	for value in czech_texts.values():
		file_path = os.path.join(folder_path, value["file_name"] + ".wav")
		print("Saving {} to {}.".format(value["czech"][0], file_path))
		tts.sayToFile(value["czech"][0], file_path)


